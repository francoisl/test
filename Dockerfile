FROM debian:buster-slim@sha256:7c459309b9a5ec1683ef3b137f39ce5888f5ad0384e488ad73c94e0243bc77d4

RUN apt-get update && apt-get install -y gnupg2=2.2.12-1+deb10u1 --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
RUN apt-get update && apt-get install -y ansible=2.7.7+dfsg-1 --no-install-recommends && rm -Rf /usr/share/doc && rm -Rf /usr/share/man && apt-get clean && rm -rf /var/lib/apt/lists/*
CMD ["ansible"]