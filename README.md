# Pipeline project

The goal is to build a basic example of continuous integration using native Gitlab CI.
This example is melting **Docker**, **Ansible** and the tool **GitLab CI/CD**.


## Content of the project

> **How it works :** Description of the files :
* A **Dockerfile** using ansible
* the configuration file **.gitlab-ci.yml**


### **CI Pipeline stages :**

  1. **Lint**
>Do a lint to check the syntax of the Dockerfile
  2. **Build**
>Build the image based on the Dockerfile
  3. **Scan**
>Vulnerability scan with trivy
### **Result :** 
The process can be checked inside the gitlab repository